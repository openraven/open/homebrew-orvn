# What

This is an Open Raven specific [Homebrew](https://brew.sh) tap
for Formulae that have not made it into the main Homebrew tap

# How

```console
$ brew tap openraven/orvn https://gitlab.com/openraven/open/homebrew-orvn.git
Cloning into '/...snip.../Library/Taps/openraven/homebrew-orvn'...
Receiving objects: 100% (5/5), done.
Tapped 1 formula (27 files, 101.4KB).
```

From that point forward, `brew install openraven/orvn/yawsso`
(or any of the names in the `Formula` directory) should behave as expected

# Uninstall?

```console
$ brew untap openraven/orvn
Untapping openraven/orvn...
Untapped 1 formula (27 files, 101.4KB).
```
