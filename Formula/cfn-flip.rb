class CfnFlip < Formula
  include Language::Python::Virtualenv

  desc "Tool for converting AWS CloudFormation templates between JSON and YAML formats."
  homepage "https://github.com/awslabs/aws-cfn-template-flip#readme"
  # appcast "https://github.com/awslabs/aws-cfn-template-flip/releases.atom"
  url "https://github.com/awslabs/aws-cfn-template-flip/archive/1.2.3.tar.gz"
  sha256 "e4ef9d8b933913d6c63408a6ca5a4b60c313b1b971e1562cd6d14ea39399754f"
  license "Apache-2.0"
  head "https://github.com/awslabs/aws-cfn-template-flip.git"

  depends_on "python"

  def install
    # https://github.com/pypa/setuptools/issues/2355#issuecomment-684836080
    ENV["SETUPTOOLS_USE_DISTUTILS"] = "stdlib"
    venv = virtualenv_create(libexec, "python3")
    system libexec/"bin/pip", "install", "-v", "-r", "requirements.txt",
                              "--ignore-installed", buildpath
    system libexec/"bin/pip", "uninstall", "-y", "cfn-flip"
    venv.pip_install_and_link buildpath
  end

  test do
    # tested the same way as awscli
    assert_match "Usage: cfn-flip ", shell_output("#{bin}/cfn-flip --help")
  end
end
