class Yawsso < Formula
  include Language::Python::Virtualenv

  desc "Yet Another AWS SSO"
  homepage "https://github.com/victorskl/yawsso#readme"
  # appcast "https://github.com/victorskl/yawsso/releases.atom"
  url "https://github.com/victorskl/yawsso/archive/0.6.1.tar.gz"
  sha256 "e97ed0f3965aabedfd471c494ce1f7a54d1fd952f72a042248f226529c3c5755"
  license "MIT"
  head "https://github.com/victorskl/yawsso.git"

  depends_on "python"

  def install
    virtualenv_install_with_resources
  end

  test do
    # tested the same way as awscli
    assert_match "Invoke aws sso login", shell_output("#{bin}/yawsso -h")
  end
end
