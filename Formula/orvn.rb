class Orvn < Formula
  desc "CLI for interacting with Open Raven Infrastructure"
  homepage "https://gitlab.com/openraven/openraven/orvn-ops-cli"
  url "git@gitlab.com:openraven/openraven/orvn-ops-cli.git", using: :git, tag: "v0.0.3"
  license "Proprietary"
  head "git@gitlab.com:openraven/openraven/orvn-ops-cli.git"

  depends_on "go" => :build

  def install
    system "go", "get"
    system "go", "build"
    bin.install "orvn"
  end
end
