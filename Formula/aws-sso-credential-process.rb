class AwsSsoCredentialProcess < Formula
  include Language::Python::Virtualenv

  desc "Bring AWS SSO-based credentials to the AWS SDKs until they have proper support"
  homepage "https://github.com/benkehoe/aws-sso-credential-process#readme"
  license "Apache-2.0"
  # appcast "https://pypi.org/rss/project/aws-sso-credential-process/releases.xml"
  url "https://files.pythonhosted.org/packages/d3/41/dfa250cbc965efb5e6e57847a0211b32f719e1e71eaa70420fc129db8749/aws-sso-credential-process-0.3.1.tar.gz"
  sha256 "1e1634185cfb8965243a5120dfbf9b828ba65b39c0634efa0ce5d7cc0f03b732"
  head "https://github.com/benkehoe/aws-sso-credential-process.git"

  depends_on "python"

  def install
    # this blows up trying to install poetry, so we have to do things the hard way
    # virtualenv_install_with_resources

    venv = virtualenv_create(libexec, Formula["python"].opt_bin/"python3")

    # we cannot use "venv.pip_install" because it includes --no-deps
    # which means we'd have to enumerate **every** poetry resource on this forumla
    system libexec/"bin/pip", "install", Pathname.pwd

    # same problem: venv.pip_install_and_link buildpath
    binaries = [
      libexec/"bin/aws-sso-credential-process",
      libexec/"bin/aws-sso-configure-profile",
      libexec/"bin/aws-configure-sso-profile"
    ]
    bin.install_symlink binaries
  end

  test do
    assert_equal version, shell_output("#{bin}/aws-sso-credential-process --version")
  end
end
