# copied wholesale from https://github.com/Homebrew/linuxbrew-core/blob/master/Formula/aws-vault.rb
class AwsVault < Formula
  desc "Securely store and access AWS credentials in development environments"
  homepage "https://github.com/99designs/aws-vault"
  license "MIT"
  # appcast "https://github.com/99designs/aws-vault/releases.atom"
  url "https://github.com/99designs/aws-vault/archive/v6.3.1.tar.gz"
  sha256 "433df90b7ed1cf1ec08aa75a4f1f993edfe5fa3fecfff5519574613ab0ab4630"
  head "https://github.com/99designs/aws-vault.git"

  depends_on "go" => :build

  def install
    flags = "-X main.Version=#{version} -s -w"

    system "go", "build", "-ldflags=#{flags}", *std_go_args

    zsh_completion.install "contrib/completions/zsh/aws-vault.zsh"
    bash_completion.install "contrib/completions/bash/aws-vault.bash"
  end

  test do
    assert_match("aws-vault: error: required argument 'profile' not provided, try --help",
      shell_output("#{bin}/aws-vault login 2>&1", 1))
  end
end
